import "./App.css";

import React, { Component } from "react";
import Button from "./components/Buttom/Button";
import Modal from "./components/Modal/Modal";

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            firstModalDisplay: false,
            secondModalDisplay: false,
        }
    }

  openFirstModal() {
    this.setState({ firstModalDisplay: true });
  }

  closeFirstModal() {
    this.setState({ firstModalDisplay: false });
  }

  openSecondModal() {
    this.setState({ secondModalDisplay: true });
  }

  closeSecondModal() {
    this.setState({ secondModalDisplay: false });
  }

  render() {
    const { firstModalDisplay, secondModalDisplay } = this.state;
    return (
      <div className="App">
        <Button
          backgroundColor="red"
          text="Open first modal"
          onClick={this.openFirstModal.bind(this)}
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          onClick={this.openSecondModal.bind(this)}
        />
          {firstModalDisplay && (<Modal
              modalDisplay={firstModalDisplay}
              text="Bla bla bla"
              closeButton={true}
              closeButtonOnClick={this.closeFirstModal.bind(this)}
              header="Some header"
              actions={[
                  <Button
                      key={1}
                      onClick={this.closeFirstModal.bind(this)}
                      className="modal-button"
                      text="Ok"
                  />,
                  <Button
                      key={2}
                      onClick={this.closeFirstModal.bind(this)}
                      className="modal-button"
                      text="Cancel"
                  />,
              ]}
          />)}
          {secondModalDisplay && (<Modal
              modalDisplay={secondModalDisplay}
              text="Another text"
              closeButton={true}
              closeButtonOnClick={this.closeSecondModal.bind(this)}
              header="Another header"
              actions={[
                  <Button
                      key={3}
                      onClick={this.closeSecondModal.bind(this)}
                      className="modal-button"
                      text="1"
                  />,
                  <Button
                      key={4}
                      onClick={this.closeSecondModal.bind(this)}
                      className="modal-button"
                      text="2"
                  />,
                  <Button
                      key={5}
                      onClick={this.closeSecondModal.bind(this)}
                      className="modal-button"
                      text="3"
                  />,
              ]}
          />)}
      </div>
    );
  }
}
